**Requirements:** 

- java 8
- maven 3 and above (for build only)

**Run**
```
java -jar app  x x ... 
```

**Notice**
```
Phone numbers may be uttered in many ways with different digit groupings (e.g. 2106930664 may
be uttered as "210 69 30 6 6 4" or "210 69 664" etc)

so x should be the number from 0 to 999
```

**Example**:
```
java -jar nni-0.0.1-SNAPSHOT-jar-with-dependencies.jar 0 0 30 69 700 24 1 3 50 2
```

**Build**
```
$ mvn clean compile assembly:single
```

**Contact**

bogdanlupashko@gmail.com
