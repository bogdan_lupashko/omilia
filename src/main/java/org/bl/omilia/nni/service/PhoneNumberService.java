package org.bl.omilia.nni.service;

import java.util.List;
import java.util.Set;

public interface PhoneNumberService {

    /**
     * The method checks for possible ambiguities in number spelling.
     * For example if the input sequence contains ‘... 20 5...’ the result number may either be ‘...205...’ or ‘...25...’.
     * Also if the sequence contains ‘...75...’, the result number may be: ‘...705...’ or ‘...75...’
     * The method identifies these ambiguities, and return any possible number interpretations.
     */
    Set<String> populatePhoneNumberVariantsFromRawNum(List<String> rawNumbers);
}
