package org.bl.omilia.nni.service;

import java.util.Map;

public interface OutputService {

    void printResults(Map<String, Boolean> printMap);

}
