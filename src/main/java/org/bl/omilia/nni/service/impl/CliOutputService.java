package org.bl.omilia.nni.service.impl;

import org.bl.omilia.nni.service.OutputService;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class CliOutputService implements OutputService {

    @Override
    public void printResults(Map<String, Boolean> printMap) {
        if (printMap == null) {
            return;
        }

        AtomicInteger i = new AtomicInteger(1);
        printMap.forEach((k, v) -> {
            String valid = v ? "VALID" : "INVALID";
            System.out.format("%1$-20s%2$-20s%3$-20s%n",
                    String.format("Interpretation %s:", i),
                    k,
                    String.format("[phone number: %s]", valid));
            i.getAndIncrement();
        });
    }

}
