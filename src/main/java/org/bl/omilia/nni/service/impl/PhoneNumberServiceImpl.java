package org.bl.omilia.nni.service.impl;

import org.bl.omilia.nni.service.PhoneNumberService;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PhoneNumberServiceImpl implements PhoneNumberService {

    @Override
    public Set<String> populatePhoneNumberVariantsFromRawNum(List<String> rawNumbers) {
        if (rawNumbers == null) {
            return null;
        }

        Set<String> result = new HashSet<>();

        for (int i = 0; i < rawNumbers.size(); i++) {
            final int nextValLength = rawNumbers.size() > i + 1 ? rawNumbers.get(i + 1).length() : 0;
            if (result.isEmpty()) {
                result.addAll(populateVal(rawNumbers.get(i), nextValLength));
                continue;
            }
            final Set<String> tempResult = new HashSet<>();
            final Set<String> finalResult = result;
            populateVal(rawNumbers.get(i), nextValLength)
                    .forEach(nextVal -> finalResult.forEach(existsVal -> tempResult.add(existsVal + nextVal)));

            result = tempResult;
        }

        return result;
    }

    private Set<String> populateVal(String value, int nextValLength) {

        final int valueLength = value.length();
        if (valueLength == 2) {
            return processTensNumber(value, nextValLength);
        } else if (valueLength == 3) {
            return processHundredsNumber(value, nextValLength);
        }

        return Collections.singleton(value);
    }

    private Set<String> processHundredsNumber(String val, int nextValLength) {

        Set<String> result = new HashSet<>();
        final int valInt = Integer.parseInt(val);
        final int tenOneIntVal = valInt % 100;

        if (tenOneIntVal == 0) {
            result.addAll(addValAndPreProcessNextVal(valInt, nextValLength));
        } else {
            Set<String> tenOneNumberResult = processTensNumber(String.valueOf(tenOneIntVal), nextValLength);

            populateHundredsHeaderVariant(val)
                    .forEach(hundreds -> tenOneNumberResult
                            .forEach(tenOnes -> result.add(hundreds + tenOnes)));
        }

        return result;
    }

    private Set<String> processTensNumber(String val, int nextValLength) {
        final int valInt = Integer.parseInt(val);

        if (valInt < 20) {
            return Collections.singleton(val);
        }

        Set<String> result = addValAndPreProcessNextVal(valInt, nextValLength);

        if (valInt % 10 != 0) {
            result.add(String.valueOf(new char[]{val.charAt(0), '0', val.charAt(1)}));
        }
        return result;
    }

    private Set<String> addValAndPreProcessNextVal(int val, int nextValLength) {
        Set<String> result = new HashSet<>();
        result.add(String.valueOf(val));

        if (val % 10 == 0 && nextValLength == 1) {
            result.add(String.valueOf(val / 10));
        }
        if (val % 100 == 0 && nextValLength == 2) {
            result.add(String.valueOf(val / 100));
        }

        return result;
    }

    private Set<String> populateHundredsHeaderVariant(String val) {
        final int valInt = Integer.parseInt(val);
        final String hundredVal = String.valueOf(val.charAt(0));

        Set<String> result = new HashSet<>();

        result.add(hundredVal + "00");
        result.add(valInt % 100 > 9 ? hundredVal : hundredVal + "0");

        return result;
    }

}
