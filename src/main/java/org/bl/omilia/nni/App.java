package org.bl.omilia.nni;

import org.bl.omilia.nni.service.OutputService;
import org.bl.omilia.nni.service.PhoneNumberService;
import org.bl.omilia.nni.service.impl.CliOutputService;
import org.bl.omilia.nni.service.impl.PhoneNumberServiceImpl;
import org.bl.omilia.nni.validator.ArgsValidator;
import org.bl.omilia.nni.validator.PhoneNumberValidator;
import org.bl.omilia.nni.validator.impl.GreekPhoneNumberValidator;
import org.bl.omilia.nni.validator.impl.ThreeDigitArgsValidator;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class App {

    public static void main(String[] args) {
        PhoneNumberService service = new PhoneNumberServiceImpl();
        ArgsValidator argsValidator = new ThreeDigitArgsValidator();
        PhoneNumberValidator pNumValidator = new GreekPhoneNumberValidator();
        OutputService outputService = new CliOutputService();

        List<String> validatedArgs = argsValidator.validateAndInitArgs(args);
        Set<String> phoneNumVariants = service.populatePhoneNumberVariantsFromRawNum(validatedArgs);
        Map<String, Boolean> pNumValidated = pNumValidator.validatePhoneNumbers(phoneNumVariants);
        outputService.printResults(pNumValidated);

    }

}
