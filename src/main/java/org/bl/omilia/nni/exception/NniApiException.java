package org.bl.omilia.nni.exception;

public class NniApiException extends RuntimeException {
    public NniApiException(String message) {
        super(message);
    }
}
