package org.bl.omilia.nni.validator.impl;

import org.bl.omilia.nni.exception.NniApiException;
import org.bl.omilia.nni.validator.ArgsValidator;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class ThreeDigitArgsValidator implements ArgsValidator {

    private static final String INPUT_FORMAT_ERROR_MESSAGE = "Phone numbers may be uttered in many ways with different digit groupings \n" +
            "(e.g. 2106930664 may be uttered as \"210 69 30 6 6 4\" or \"210 69 664\" etc)";

    @Override
    public List<String> validateAndInitArgs(String[] args) throws NniApiException {

        if (args == null || args.length == 0) {
            throw new NniApiException(INPUT_FORMAT_ERROR_MESSAGE);
        }

        List<String> result = Arrays.asList(args);

        result.forEach(arg -> {
            // allow digits only
            if (!Pattern.matches("^[0-9]*$", arg)
                    || arg.length() > 3
                    || (arg.length() > 1 && arg.startsWith("0"))) {
                throw new NniApiException(INPUT_FORMAT_ERROR_MESSAGE);
            }
        });

        return result;
    }

}
