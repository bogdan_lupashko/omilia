package org.bl.omilia.nni.validator.impl;

import org.bl.omilia.nni.validator.PhoneNumberValidator;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Assume that valid Greek phone numbers may have 10 or 14 digits. If they have 10 digits, they must start
 * with ‘2’ or ‘69’. If they have 14 digits, the must start with ‘00302’ or ‘003069’.
 *  For example, if input is: 30 2 5 58, output should be: 302558 [phone number: INVALID]
 *  If input is: 2 10 69 30 6 6 4, output should be: 2106930664 [phone number: VALID]
 *  If input is: 2 10 69 30 6 60 4, output should be: 21069306604 [phone number: INVALID]
 *  If input is: 0 0 30 69 74 0 9 22 52, output should be: 00306974092252 [phone number: VALID]
 */
public class GreekPhoneNumberValidator implements PhoneNumberValidator {

    @Override
    public Map<String, Boolean> validatePhoneNumbers(Set<String> phoneNumbers) {
        if (phoneNumbers == null) {
            return null;
        }

        Map<String, Boolean> result = new HashMap<>();

        phoneNumbers.forEach(pNum -> {
            final boolean isValid = (pNum.length() == 10 && (pNum.startsWith("2") || pNum.startsWith("69")))
                    || (pNum.length() == 14 && (pNum.startsWith("00302") || pNum.startsWith("003069")));

            result.put(pNum, isValid);
        });

        return result;
    }
}
