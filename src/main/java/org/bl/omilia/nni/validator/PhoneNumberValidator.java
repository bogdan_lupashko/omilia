package org.bl.omilia.nni.validator;

import java.util.Map;
import java.util.Set;

public interface PhoneNumberValidator {

    Map<String, Boolean> validatePhoneNumbers(Set<String> phoneNumbers);

}
