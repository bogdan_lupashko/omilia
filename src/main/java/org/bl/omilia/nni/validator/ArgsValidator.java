package org.bl.omilia.nni.validator;

import org.bl.omilia.nni.exception.NniApiException;

import java.util.List;

public interface ArgsValidator {

    /**
     * Phone numbers may be uttered in many ways with different digit groupings (e.g. 2106930664 may
     * be uttered as "210 69 30 6 6 4" or "210 69 664" etc).
     */
    List<String> validateAndInitArgs(String[] args) throws NniApiException;

}
