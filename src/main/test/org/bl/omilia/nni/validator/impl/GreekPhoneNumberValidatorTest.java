package org.bl.omilia.nni.validator.impl;

import org.bl.omilia.nni.validator.PhoneNumberValidator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class GreekPhoneNumberValidatorTest {

    private PhoneNumberValidator validator;

    @Before
    public void setUp() {
        validator = new GreekPhoneNumberValidator();
    }

    @After
    public void tearDown() {
        validator = null;
    }

    @Test
    public void validatePhoneNumbers1() {

        final Set<String> pNums = Collections.singleton("003069720413506");

        Map<String, Boolean> result = validator.validatePhoneNumbers(pNums);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertFalse(result.get("003069720413506"));
    }

    @Test
    public void validatePhoneNumbers2() {

        Map<String, Boolean> result = validator.validatePhoneNumbers(new HashSet<>());

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    public void validatePhoneNumbers3() {

        Map<String, Boolean> result = validator.validatePhoneNumbers(null);

        assertNull(result);
    }

    @Test
    public void validatePhoneNumbers4() {

        final Set<String> pNums = Collections.singleton("00306972041356");

        Map<String, Boolean> result = validator.validatePhoneNumbers(pNums);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertTrue(result.get("00306972041356"));
    }

    @Test
    public void validatePhoneNumbers5() {

        final Set<String> pNums = Collections.singleton("00302720413566");

        Map<String, Boolean> result = validator.validatePhoneNumbers(pNums);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertTrue(result.get("00302720413566"));
    }

    @Test
    public void validatePhoneNumbers6() {

        final Set<String> pNums = Collections.singleton("2720413566");

        Map<String, Boolean> result = validator.validatePhoneNumbers(pNums);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertTrue(result.get("2720413566"));
    }

    @Test
    public void validatePhoneNumbers7() {

        final Set<String> pNums = Collections.singleton("6972041356");

        Map<String, Boolean> result = validator.validatePhoneNumbers(pNums);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertTrue(result.get("6972041356"));
    }

    @Test
    public void validatePhoneNumbers8() {

        final Set<String> pNums = Collections.singleton("69720413568");

        Map<String, Boolean> result = validator.validatePhoneNumbers(pNums);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertFalse(result.get("69720413568"));
    }

    @Test
    public void validatePhoneNumbers9() {

        final Set<String> pNums = Collections.singleton("27204135668");

        Map<String, Boolean> result = validator.validatePhoneNumbers(pNums);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertFalse(result.get("27204135668"));
    }

    @Test
    public void validatePhoneNumbers10() {

        final Set<String> pNums = Collections.singleton("272041356");

        Map<String, Boolean> result = validator.validatePhoneNumbers(pNums);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertFalse(result.get("272041356"));
    }

    @Test
    public void validatePhoneNumbers11() {

        final Set<String> pNums = Collections.singleton("697204135");

        Map<String, Boolean> result = validator.validatePhoneNumbers(pNums);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertFalse(result.get("697204135"));
    }

    @Test
    public void validatePhoneNumbers12() {

        final Set<String> pNums = Collections.singleton("003069720413568");

        Map<String, Boolean> result = validator.validatePhoneNumbers(pNums);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertFalse(result.get("003069720413568"));
    }

    @Test
    public void validatePhoneNumbers13() {

        final Set<String> pNums = Collections.singleton("003027204135668");

        Map<String, Boolean> result = validator.validatePhoneNumbers(pNums);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertFalse(result.get("003027204135668"));
    }

    @Test
    public void validatePhoneNumbers14() {

        final Set<String> pNums = Collections.singleton("0030697204135");

        Map<String, Boolean> result = validator.validatePhoneNumbers(pNums);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertFalse(result.get("0030697204135"));
    }

    @Test
    public void validatePhoneNumbers15() {

        final Set<String> pNums = Collections.singleton("0030272041356");

        Map<String, Boolean> result = validator.validatePhoneNumbers(pNums);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertFalse(result.get("0030272041356"));
    }

}