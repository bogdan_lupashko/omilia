package org.bl.omilia.nni.validator.impl;

import org.bl.omilia.nni.exception.NniApiException;
import org.bl.omilia.nni.validator.ArgsValidator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ThreeDigitArgsValidatorTest {

    private ArgsValidator validator;

    @Before
    public void setUp() {
        validator = new ThreeDigitArgsValidator();
    }

    @After
    public void tearDown() {
        validator = null;
    }

    @Test(expected = NniApiException.class)
    public void validateAndInitArgs1() {

        final String[] args = new String[]{"20", "d"};

        validator.validateAndInitArgs(args);
    }

    @Test(expected = NniApiException.class)
    public void validateAndInitArgs2() {
        validator.validateAndInitArgs(null);
    }

    @Test(expected = NniApiException.class)
    public void validateAndInitArgs3() {
        validator.validateAndInitArgs(new String[]{});
    }

    @Test
    public void validateAndInitArgs4() {

        final String[] args = new String[]{"20", "50"};

        final List<String> result = validator.validateAndInitArgs(args);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.contains("20"));
        assertTrue(result.contains("50"));
    }

    @Test(expected = NniApiException.class)
    public void validateAndInitArgs5() {

        final String[] args = new String[]{"20", "-1"};

        validator.validateAndInitArgs(args);
    }

    @Test(expected = NniApiException.class)
    public void validateAndInitArgs6() {

        final String[] args = new String[]{"20", "+1"};

        validator.validateAndInitArgs(args);
    }

    @Test
    public void validateAndInitArgs7() {

        final String[] args = new String[]{"0", "1"};

        final List<String> result = validator.validateAndInitArgs(args);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.contains("0"));
        assertTrue(result.contains("1"));
    }

    @Test(expected = NniApiException.class)
    public void validateAndInitArgs8() {

        final String[] args = new String[]{"78", "4000"};

        validator.validateAndInitArgs(args);
    }

    @Test(expected = NniApiException.class)
    public void validateAndInitArgs9() {

        final String[] args = new String[]{"78", "026"};

        validator.validateAndInitArgs(args);
    }

}

