package org.bl.omilia.nni.service.impl;

import org.bl.omilia.nni.service.PhoneNumberService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PhoneNumberServiceImplTest {

    private PhoneNumberService service;

    @Before
    public void setUp() {
        service = new PhoneNumberServiceImpl();
    }

    @After
    public void tearDown() {
        service = null;
    }

    @Test
    public void populateVariousNumsTest1() {

        final List<String> args = Collections.singletonList("25");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.contains("25"));
        assertTrue(result.contains("205"));

    }

    @Test
    public void populateVariousNumsTest2() {

        final List<String> args = Arrays.asList("20", "5");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.contains("25"));
        assertTrue(result.contains("205"));

    }

    @Test
    public void populateVariousNumsTest3() {

        final List<String> args = Arrays.asList("20", "51");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.contains("2051"));
        assertTrue(result.contains("20501"));

    }

    @Test
    public void populateVariousNumsTest4() {

        final List<String> args = Arrays.asList("0", "5");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertTrue(result.contains("05"));

    }

    @Test
    public void populateVariousNumsTest5() {

        final List<String> args = Arrays.asList("10", "5");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertTrue(result.contains("105"));
        assertFalse(result.contains("15"));

    }

    @Test
    public void populateVariousNumsTest6() {

        final List<String> args = Arrays.asList("100", "5");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.contains("1005"));
        assertTrue(result.contains("105"));

    }

    @Test
    public void populateVariousNumsTest7() {

        final List<String> args = Arrays.asList("300", "52");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(4, result.size());
        assertTrue(result.contains("300502"));
        assertTrue(result.contains("30052"));
        assertTrue(result.contains("3502"));
        assertTrue(result.contains("352"));

    }

    @Test
    public void populateVariousNumsTest8() {

        final List<String> args = Arrays.asList("300", "50", "2");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(4, result.size());
        assertTrue(result.contains("300502"));
        assertTrue(result.contains("30052"));
        assertTrue(result.contains("3502"));
        assertTrue(result.contains("352"));

    }

    @Test
    public void populateVariousNumsTest9() {

        final List<String> args = Collections.singletonList("352");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(4, result.size());
        assertTrue(result.contains("300502"));
        assertTrue(result.contains("30052"));
        assertTrue(result.contains("3502"));
        assertTrue(result.contains("352"));

    }

    @Test
    public void populateVariousNumsTest10() {

        final List<String> args = Arrays.asList("350", "2");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(4, result.size());
        assertTrue(result.contains("300502"));
        assertTrue(result.contains("30052"));
        assertTrue(result.contains("3502"));
        assertTrue(result.contains("352"));

    }

    @Test
    public void populateVariousNumsTest11() {

        final List<String> args = Arrays.asList("100", "10", "6");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.contains("1106"));
        assertTrue(result.contains("100106"));

    }

    @Test
    public void populateVariousNumsTest12() {

        final List<String> args = Arrays.asList("110", "6");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.contains("1106"));
        assertTrue(result.contains("100106"));

    }

    @Test
    public void populateVariousNumsTest13() {

        final List<String> args = Arrays.asList("320", "2");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(4, result.size());
        assertTrue(result.contains("300202"));
        assertTrue(result.contains("30022"));
        assertTrue(result.contains("3202"));
        assertTrue(result.contains("322"));

    }

    @Test
    public void populateVariousNumsTest14() {

        final List<String> args = Arrays.asList("17", "3");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertTrue(result.contains("173"));

    }

    @Test
    public void populateVariousNumsTest15() {

        final List<String> args = Collections.singletonList("109");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.contains("109"));
        assertTrue(result.contains("1009"));

    }

    @Test
    public void populateVariousNumsTest16() {

        final List<String> args = Arrays.asList("19", "5");

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(args);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertTrue(result.contains("195"));

    }

    @Test
    public void populateVariousNumsTest17() {

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(new ArrayList<>());

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    public void populateVariousNumsTest18() {

        final Set<String> result = service.populatePhoneNumberVariantsFromRawNum(null);

        assertNull(result);
    }

}